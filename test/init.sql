CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE SCHEMA "usermanager";

CREATE TABLE "usermanager"."People" (
    "id" uuid DEFAULT uuid_generate_v4() NOT NULL,
    "firstName" character varying NOT NULL,
    "lastName" character varying NOT NULL,
    "email" character varying NOT NULL,
    "city" character varying NOT NULL,
    "createdAt" timestamp DEFAULT now(),
    "updatedAt" timestamp,
    "username" character varying,
    "birthday" character varying,
    "attachmentId" character varying,
    CONSTRAINT "person_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

commit;
