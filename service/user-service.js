const repository = require('../repository/user-repository');
const userGrpRepository = require('../repository/user-group-repository');
const userFollowRepository = require('../repository/user-follow-repository');

require('dotenv').config({path:'../.env'});
const rp = require('request-promise');
const JSEncrypt = require('node-jsencrypt')
const jwt = require('jsonwebtoken')
const err = require('../model/errors');


module.exports = {
    createUser: function(data){
        return new Promise((resolve, reject) => {
            getAdminToken()
            .then((result) => {
                var convertedResult = JSON.parse(result)
                if(convertedResult === undefined)
                    reject('Empty access token')
                decryptPwd(data.password)
                .then((result)=> {
                    var convertedRes = ''
                    convertedRes = result
                    postKeycloakUser(convertedResult["access_token"],data,result)
                    .then((idUserkeycloak) => {
                        repository.createUser(idUserkeycloak,data)
                        .then(() => resolve('User Created'))
                        .catch((error) => reject(error))
                    })
                    .catch((error) => reject(error))
                })
                .catch((error)=> reject(error))
            })
            .catch((error) => reject(error))
            })
    },
    readUsers: function() {
        return new Promise((resolve, reject) => {
            repository.getUsers()
                .then((result) => resolve(result))
                .catch((error) => reject(error))
        })
    },

    readUser: function(id) {
        return new Promise((resolve, reject) => {
            repository.getUser(id)
            .then((result) => resolve(result))
            .catch((error) => reject(error))
        })
    },

    updateUser: function(id, data) {
        return new Promise((resolve, reject) => {
            getAdminToken()
            .then((resToken)=>{
                var resConvertedToken = JSON.parse(resToken)
                console.log('prod get admin token passed')
                if(resConvertedToken === undefined)
                    reject('Empty admin access token')
                updateKeycloakUser(id,resConvertedToken["access_token"],data)
                .then(()=>{
                    console.log('prod update then')
                    repository.updateUser(id, data)
                    .then((result) => resolve(result))
                    .catch((error) => reject(error))
                })
                .catch((error)=>{
                    reject(error)
                })
            })
            .catch((err)=>{
                reject(err)
            })
        })
    },
    deleteUser: function(id) {
        return new Promise((resolve, reject) => {
            repository.deleteUser(id)
            .then((result) => resolve(result))
            .catch((error) => reject(error))
        })
    },
    getUserGroups: function(id) {
        return new Promise((resolve, reject) => {
            userGrpRepository.getUserGroups(id)
                .then((result) => resolve(result))
                .catch((error) => reject(error))
        })
    },
}

async function decryptPwd(encryptedPwd){
    return new Promise((resolve,reject)=>{
        let decrypt = new JSEncrypt()
        let privateKey = process.env.PRIVATE_KEY 
        decrypt.setPrivateKey(privateKey)
        var decryptedPwd = decrypt.decrypt(encryptedPwd)
        resolve(decryptedPwd);
    })

}

async function postKeycloakUser(adminToken,data,pwd){
    pwd = pwd.substr(1,pwd.length-2)
    return new Promise((resolve,reject)=>{
        var options = {
            method: 'POST',
            uri: process.env.KEYCLOAK_URL_CREATE_U,
            headers:{
                'Authorization': 'Bearer '+adminToken
            },
            body: {
                firstName: data.firstName,
                lastName: data.lastName,
                email: data.email,
                username: data.username,
                enabled: true,
                credentials: [{
                    type: 'password',
                    value: pwd
                }]
            },
            json: true // Automatically stringifies the body to JSON
        };
        rp(options)
        .then(() => {
            var optionsAllUsers = {
                method: 'GET',
                uri: "http://keycloak.vitisb.fr/auth/admin/realms/keycloak-user-manager/users",
                headers:{
                    'Authorization': 'Bearer '+adminToken
                }
            }
            rp(optionsAllUsers)
            .then((res)=>{
                var jsonRes = JSON.parse(res);
                for(var i in jsonRes){
                    if(data.username.toLowerCase() == jsonRes[i].username){
                        resolve(jsonRes[i].id)
                    }
                }
            })
            .catch((err)=>{
                reject(err)
            })
        })
        .catch((error) => {
            reject(error)
        })
    })
}

async function updateKeycloakUser(userId,adminToken,data){
    return new Promise((resolve,reject)=>{
        var options = {
            method: 'PUT',
            uri: process.env.KEYCLOAK_URL_CREATE_U+'/'+userId,
            headers:{
                'Authorization': 'Bearer '+adminToken
            },
            body: {
                firstName: data.firstName,
                lastName: data.lastName
            },
            json: true
        };
        rp(options)
        .then(() => {
            resolve('User updated')
        })
        .catch((error) => {
            reject(error)
        })
    })
}

// May be useful if you want to retrieve information from keycloak 
async function getAdminToken(){
    return new Promise((resolve,reject) => {
        rp.post(process.env.KEYCLOAK_URL_A,{
            form:{
                client_id: process.env.CLIENT_ID_A,
                username: process.env.USERNAME_KEYCLOAK_A,
                password: process.env.PASSWORD_KEYCLOAK_A,
                grant_type: process.env.GRANT_TYPE_A
            }
        })
        .then((result) =>{
            console.log('get admin token before resolve')
            resolve(result)
        })
        .catch((error) =>{
            console.log('get admin token before reject')
            reject(error)
        }) 
    })
}
// Get user information from his access token which is decoded by jwt 
async function getUser(token){
    return new Promise((resolve,reject) => {
        if(token === undefined)
            throw(new err.Error(err.errors.INVALID_TOKEN, 'Token invalid'));
        let tokenDecoded = jwt.decode(token)
        let arrayInfoUser = new Object()
        arrayInfoUser.preferred_username = tokenDecoded['preferred_username']
        arrayInfoUser.given_name = tokenDecoded['given_name']
        arrayInfoUser.family_name = tokenDecoded['family_name']
        arrayInfoUser.email = tokenDecoded['email']
        arrayInfoUser.city = 'Paris'
        resolve(arrayInfoUser)
    })
}
