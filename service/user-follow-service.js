const userFollowRepository = require('../repository/user-follow-repository');
require('dotenv').config({path:'../.env'});
const rp = require('request-promise');
const jwt = require('jsonwebtoken')
const err = require('../model/errors');

module.exports = {
    createFollowing: function(body){
        console.log('service');
        console.log(body)
        return new Promise((resolve,reject)=>{
            userFollowRepository.createFollowing(body.idUser,body.idUserFollowed)
            .then((result) => resolve(result))
            .catch((error) => reject(error))
        })
    },
    deleteFollowing: function(id,body){
        return new Promise((resolve,reject)=>{
            userFollowRepository.deleteFollowing(id,body.idUserFollowed)
            .then((result) => resolve(result))
            .catch((error) => reject(error))
        })
    },
    getFollowing: function(id,idFollowed){
        return new Promise((resolve,reject)=>{
            userFollowRepository.getFollowing(id,idFollowed)
            .then((result) => resolve(result))
            .catch((error) => reject(error))
        })
    }
}
