require('dotenv').config({path:'../.env'})
module.exports = {
    kcConfig : {
       serverUrl: process.env.KEYCLOAK_SERVER_URL,
       bearerOnly: true,
       clientId: process.env.CLIENT_ID, 
       realm: process.env.REALM_KEYCLOAK,
       realmPublicKey: process.env.REALM_KEYCLOAK_PK
   }
}