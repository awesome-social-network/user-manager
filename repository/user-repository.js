const people = require('../model/user');
const peopleFollow = require('../model/user-follow');
const err = require('../model/errors');

const SUC = { 'result': 'ok' }
const ERR = { 'result': 'error' }

module.exports = {

    getUsers: function () {
        return new Promise((resolve, reject) => {
            people.findAll()
                .then(users => {
                    resolve(JSON.stringify(users, null, 4))
                })
                .catch(error => {
                    console.log("Publication.findAll().error(): " + error)
                    reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
                })
        })
    },

    getUser: function (id) {
        return new Promise((resolve, reject) => {
            people.findOne({
                where: {
                    id: id
                }
            }).then(users => {
                resolve(JSON.stringify(users, null, 4))
            }).catch(error => {
                console.log("Publication.findAll().error(): " + error)
                reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
            })

        })
    },

    createUser: function (idUserKeycloak, data) {
        return new Promise((resolve, reject) => {
            people.create({
                id: idUserKeycloak,
                lastName: data.lastName,
                firstName: data.firstName,
                email: data.email,
                city: data.city,
                birthday: data.birthday,
                username: data.username
            }).then(function () {
                resolve('User has been created')
            }).catch(error => {
                console.log("Publication.findAll().error(): " + error)
                reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
            })
        })
    },

    deleteUser: function (id) {
        return new Promise((resolve, reject) => {
            people.destroy({
                where: {
                    id: id
                }
            }).then(
                resolve(SUC)
            ).catch(error => {
                console.log("Publication.findAll().error(): " + error)
                reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
            })
        })
    },
    updateUser: function (id, data) {
        return new Promise((resolve, reject) => {
            console.log('update user')
            console.log(data)
            if (data.attachmentId == null) {
                console.log('dans if repo')
                people.update({
                    lastName: data.lastName,
                    firstName: data.firstName,
                    email: data.email,
                    city: data.city,
                    updatedAt: Date.now()
                }, {
                    where: {
                        id: id
                    }
                }).then(
                    resolve(SUC)
                ).catch(error => {
                    console.log("Publication.findAll().error(): " + error)
                    reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
                })
            }
            else {
                console.log('dans else repo')
                people.update({
                    lastName: data.lastName,
                    firstName: data.firstName,
                    email: data.email,
                    city: data.city,
                    attachmentId: data.attachmentId,
                    updatedAt: Date.now()
                }, {
                    where: {
                        id: id
                    }
                }).then(
                    resolve(SUC)
                ).catch(error => {
                    console.log("Publication.findAll().error(): " + error)
                    reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
                })
            }
        })
    }

}