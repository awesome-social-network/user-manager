const err = require('../model/errors');
const peopleFollow = require('../model/user-follow');

module.exports = {
    createFollowing: function(idUser,idUserFollowed){
        return new Promise((resolve,reject)=>{
            peopleFollow.create({
                peopleId: idUser,
                peopleFollowedId: idUserFollowed
            })
            .then((res)=> {
                resolve('User is followed')
            })
            .catch((error)=>{
                reject(error)
            }) 
        })
    },
    deleteFollowing: function(idUser,idUserFollowed){
        return new Promise((resolve,reject)=>{
            peopleFollow.destroy({
                where: {
                    peopleId: idUser,
                    peopleFollowedId: idUserFollowed
                }
            })
            .then((res)=> resolve('follow deleted'))
            .catch((err)=> reject(err))
        })
    },
    getFollowing: function(idUser,idUserFollowed){
        return new Promise((resolve,reject)=>{
            peopleFollow.findOne({
                where:{
                    peopleId: idUser,
                    peopleFollowedId: idUserFollowed
                }
            })
            .then((res)=>resolve(res))
            .catch((err)=>reject(err))
        })   
    }
}
