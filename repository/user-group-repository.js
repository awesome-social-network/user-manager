const err = require('../model/errors');
const peopleGroup = require('../model/user-group');

module.exports = {
    getUserGroups: function(id) {
        return new Promise((resolve, reject) => {
            peopleGroup.findAll({
                where: {
                    peopleId: id
                }
            })
                .then(users =>{
                    resolve(JSON.stringify(users, null, 4))
                })
                .catch(error => {
                    reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
                })
        })
    }

}