const service = require('../service/user-service');
require('dotenv').config({path:'../.env'})
const err = require('../model/errors');

module.exports = {

    getUsers: function(req, res) {
        service.readUsers()
            .then((result) => res.status(200).send(result))
            .catch((error) => res.status(500).send(error))
    },

    getUser: function(req, res) {
        service.readUser(req.params.id)
            .then((result) => res.status(200).send(result))
            .catch((error) => res.status(500).send(error))
    },

    createUser: function(req, res) {
        service.createUser(req.body)
            .then((result) => res.status(201).send(result))
            .catch((error) => {
                if(error instanceof err.Error) {
                    if(error.code === err.errors.INVALID_REQUEST) {
                        res.status(400).send(error)
                    }else {
                        res.status(500).send(error)
                    }
                } else {
                    res.status(500).send(error)
                }
            })
    },
    deleteUser: function(req, res) {
        service.deleteUser(req.params.id)
            .then((result) => res.status(200).send(result))
            .catch((error) => res.status(500).send(error))
    },

   updateUser: function(req, res) {
        service.updateUser(req.params.id, req.body)
            .then((result) => res.status(201).send(result))
            .catch((error) => res.status(500).send(error))
    },

    getUserGroups: function(req, res) {
        service.getUserGroups(req.params.id)
            .then((result) => res.status(201).send(result))
            .catch((error) => res.status(500).send(error))
    }

};