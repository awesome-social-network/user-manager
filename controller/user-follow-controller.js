const service = require('../service/user-follow-service');
require('dotenv').config({path:'../.env'})
const err = require('../model/errors');

module.exports = {
    createFollowing: function(req,res){
        console.log(req.body)
        console.log('controller');
        service.createFollowing(req.body)
        .then((result)=> res.status(201).send(result))
        .catch((error)=>{
            res.status(500).send(error)
        })
    },
    deleteFollowing: function(req,res){
        service.deleteFollowing(req.params.id,req.body)
        .then((result)=> res.status(200).send(result))
        .catch((error)=>{
            res.status(500).send(error)
        })
    },
    getFollowing: function(req,res){
        service.getFollowing(req.params.id,req.params.idFollow)
        .then((result)=> res.status(200).send(result))
        .catch((error)=>{
            res.status(500).send(error)
        })
    }
}
