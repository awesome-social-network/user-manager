FROM node:11-alpine
WORKDIR /app
COPY package.json /app
COPY swagger.json /app
RUN npm install
RUN npm install swagger-ui-express
RUN npm install jsencrypt
COPY . /app
CMD node main.js
EXPOSE 8200