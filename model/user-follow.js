const Sequelize = require('sequelize');
const database = require('../config/database-config').database;

const PeopleFollow = database.define('PeopleFollow', {
    peopleId: {
        foreignKey: true,
        type: Sequelize.UUID,
        allowNull: false
    },
    peopleFollowedId: {
        foreignKey: true,
        type: Sequelize.UUID
    },
    updatedAt: {
        type: Sequelize.DATE,
    },
    createdAt: {
        type: Sequelize.DATE,
    }
  }, {
    // options
  });
PeopleFollow.removeAttribute('id');
module.exports = PeopleFollow;