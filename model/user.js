const Sequelize = require('sequelize');
const database = require('../config/database-config').database;

/**
 * @swagger
 * definitions:
 *   User:
 *     properties:
 *       firstName:
 *         type: string
 *       lastName:
 *         type: string
 *       email:
 *         type: string
 *         format: email
 *       city:
 *         type: string
 *   CompleteUser:
 *     allOf:
 *       - $ref: "#/definitions/User"
 *       - type: object
 *         properties:
 *           id:
 *             type: string
 *             format: uuid
 *           updatedAt:
 *             type: string
 *             format: timestamp
 *           createdAt:
 *             type: string
 *             format: timestamp   
 */
const People = database.define('People', {
    id: {
        primaryKey: true,
        type: Sequelize.UUID,
    },
    firstName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    lastName: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    city:{
        type: Sequelize.STRING
    },
    updatedAt: {
        type: Sequelize.DATE,
    },
    createdAt: {
        type: Sequelize.DATE,
    },
    birthday: {
        type: Sequelize.STRING
    },
    username: {
        type: Sequelize.STRING
    },
    attachmentId:{
      type: Sequelize.STRING
    }
  }, {
    // options
  });

  module.exports = People;