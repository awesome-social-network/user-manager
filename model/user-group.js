const Sequelize = require('sequelize');
const database = require('../config/database-config').database;

const PeopleGroup = database.define('PeopleGroup', {
    peopleId: {
        foreignKey: true,
        type: Sequelize.UUID,
    },
    groupId: {
        foreignKey: true,
        type: Sequelize.UUID,
        allowNull: false
    },
    updatedAt: {
        type: Sequelize.DATE,
    },
    createdAt: {
        type: Sequelize.DATE,
    }
  }, {
    // options
  });
PeopleGroup.removeAttribute('id');
module.exports = PeopleGroup;