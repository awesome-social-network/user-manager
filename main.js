const uuidRegex = '/:id([0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12})'
const bodyParser = require('body-parser')
const swaggerUi = require('swagger-ui-express')
const global = require('./config/config')
require('dotenv').config({path:'.env'})
const keycloakConfig = require('./config/keycloak')
const memoryStore = new global.session.MemoryStore();
const keycloak = new global.keycloak({ store: memoryStore },keycloakConfig.kcConfig)
const userController = require('./controller/user-controller');
const userFollowController = require('./controller/user-follow-controller');
const swaggerDocument = require('./swagger.json');
const express = require('express');
const app = express();
const cors = require("cors");

var corsOptions = {
    origin: 'http://social-network.vitisb.fr',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
app.use(bodyParser.json());

app.use(global.session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    store: memoryStore
  }));
app.use(keycloak.middleware());

if(process.env.NODE_ENV === 'production') {
    app.use(cors(corsOptions))
} else {
    app.use(cors())
}

app.get('/users',keycloak.protect(), function(req, res) {
    userController.getUsers(req, res)
});

app.get('/users/:id',keycloak.protect(), function(req, res) {
    userController.getUser(req, res)
});

app.post('/users', function(req, res) {
    console.log(req.body)
    userController.createUser(req, res)
});

app.post('/users/follows',keycloak.protect(),function(req,res){
    console.log('main');
    userFollowController.createFollowing(req,res)
});

app.delete('/users/follows/:id',keycloak.protect(), function(req,res){
    userFollowController.deleteFollowing(req,res)
});

app.get('/users/follows/:id&:idFollow',keycloak.protect(), function(req,res){
    userFollowController.getFollowing(req,res)
})

app.patch('/users/:id',keycloak.protect(), function(req, res) {
    userController.updateUser(req, res)
});

app.delete('/users/:id',keycloak.protect(), function(req, res) {
    userController.deleteUser(req, res)
});

app.get('/users/:id/groups', keycloak.protect(), function (req, res) {
    userController.getUserGroups(req, res)
})

app.use('/api-docs', swaggerUi.serve,swaggerUi.setup(swaggerDocument));

app.listen(8200, function () {
    console.log('app listening on port 8200!')
});